def load(file_path: str):
    elves = ""

    with open(file_path, 'r') as file:
        elves = file.read()
    
    elves = elves.split("\n\n")

    stringed_numbers = [ elf.split("\n") for elf in elves ]

    return [ [ int(number) for number in numbers ] for numbers in stringed_numbers ]

def sum_nested_sorted(calorie_list: list):
    sum_list = []

    for element in calorie_list:
        sum_list.append(sum(element))

    sum_list.sort(reverse=True)

    return sum_list

def main():
    elves = load('input/day1')

    summed_elves_calories = sum_nested_sorted(elves)
    top_calories = max(summed_elves_calories)

    print(summed_elves_calories)

    top_three = [ item[1] for item in filter(lambda item : item[0] < 3, enumerate(summed_elves_calories))]

    print(top_three)

    print(top_calories)
    print(sum(top_three))

if __name__ == '__main__':
    main()
