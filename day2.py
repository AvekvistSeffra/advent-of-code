ABC_TO_MOVE = {
    'A': 'Rock',
    'B': 'Paper',
    'C': 'Scissors',
}

XYZ_TO_MOVE = {
    'X': 'Rock',
    'Y': 'Paper',
    'Z': 'Scissors',
}

MOVE_TO_POINTS = {
    'Rock': 1,
    'Paper': 2,
    'Scissors': 3,
}

XYZ_TO_OUTCOME_TYPE = {
    'X': 'Lose',
    'Y': 'Draw',
    'Z': 'Win,'
}


def load(file_path: str) -> list:
    strategy_guide = []

    with open(file_path, 'r') as file:
        strategy_guide = file.readlines()

    strategy_guide = [entry.split(' ') for entry in strategy_guide]
    strategy_guide = [(entry[0], entry[1].replace('\n', ''))
                      for entry in strategy_guide]

    return strategy_guide


def outcome(opponent: str, me: str) -> int:
    # Check if there's a draw
    if opponent == me:
        return 3

    # Check if we lose
    if opponent == "Rock" and me == "Scissors" or opponent == "Scissors" and me == "Paper" or opponent == "Paper" and me == "Rock":
        return 0

    # If we don't get a draw and don't lose, we win
    return 6


def get_strategy_move(opponent: str, outcome_type: str) -> str:
    match XYZ_TO_OUTCOME_TYPE[outcome_type]:
        case 'Lose':
            match opponent:
                case 'Rock':
                    return 'Scissors'
                case 'Paper':
                    return 'Rock'
                case 'Scissors':
                    return 'Paper'
        case 'Draw':
            return opponent
        case 'Win':
            match opponent:
                case 'Rock':
                    return 'Paper'
                case 'Paper':
                    return 'Scissors'
                case 'Scissors':
                    return 'Rock'


def get_points(game: tuple) -> int:
    opponent, me = game

    print(opponent, '|', me)

    me = XYZ_TO_MOVE[me]
    opponent = ABC_TO_MOVE[opponent]

    print(opponent, '|', me)

    points = outcome(opponent, me)
    me = MOVE_TO_POINTS[me]

    print(points, '|', me)

    print(5 * '-')

    return points + me

def get_points_with_outcome(game: tuple) -> int:
    opponent, outcome_type = game

    print(opponent, '|', outcome_type)

    opponent = ABC_TO_MOVE[opponent]
    me = get_strategy_move(opponent, outcome_type)

    print(opponent, '|', me)

    points = outcome(opponent, me)
    me = MOVE_TO_POINTS[me]

    print(points, '|', me)

    print(5 * '-')

    return points + me

def main():
    strategy_guide = load('input/day2')

    total_points = 0

    for game in strategy_guide:
        total_points += get_points_with_outcome(game)

    print(total_points)


if __name__ == '__main__':
    main()
